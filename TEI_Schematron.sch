<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt3"
    xmlns:sqf="http://www.schematron-quickfix.com/validator/process"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    
    <sch:ns uri="http://www.tei-c.org/ns/1.0" prefix="tei" />
    <sch:pattern>
        <sch:rule context="tei:titleStmt">
            
            <sch:let name="title-count" value="count(tei:title)"/>
            <sch:assert test="$title-count eq 1">
                There should be only one title element within a titleStmt element.
            </sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="tei:body">
            
            <sch:assert test="count(*) gt 0">
                The body element is empty.
            </sch:assert>
            
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="tei:front">
            
            <sch:assert test="count(*) gt 0">
                The front element is empty.
            </sch:assert>
            
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="tei:back">
            
            <sch:assert test="count(*) gt 0">
                The back element is empty.
            </sch:assert>
            
        </sch:rule>
    </sch:pattern>
    
    <!-- This patten can currently find spaces that are in between the xml:id attribute but not starting or ending spaces -->
    <sch:pattern>
        <sch:rule context="*[@xml:id]">
            <sch:let name="hasSpacesInBetween" value="contains(@xml:id, ' ')"/>
            <sch:let name="hasLeadingSpace" value="starts-with(@xml:id, ' ')"/>
            <sch:let name="hasTrailingSpace" value="ends-with(@xml:id, ' ')"/>
            <sch:assert test="not($hasLeadingSpace or $hasSpacesInBetween or $hasTrailingSpace)">
                The 'xml:id' attribute in element <sch:value-of select="local-name()"/> must not contain spaces. Found space in: '<sch:value-of select="@xml:id"/>'.
            </sch:assert>
        </sch:rule>
    </sch:pattern>
    
    <sch:pattern>
        <sch:rule context="tei:date">
            <sch:assert test="matches(@when, '^[0-9]{4}[-][0-9]{2}[-][0-9]{2}$') or matches(@when, '^[0-9]{4}$')">
                The 'when' attribute in element <sch:value-of select="local-name()"/> is not properly formatted. Found error in: '<sch:value-of select="@when"/>'.
            </sch:assert>
        </sch:rule>
    </sch:pattern>
</sch:schema>
<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
   xmlns:xs="http://www.w3.org/2001/XMLSchema"
   xmlns:xd="https://www.w3.org/2001/XMLSchema"
   xmlns:fn="http://www.w3.org/2005/xpath-functions"
   xmlns:mml="http://www.w3.org/1998/Math/MathML"
   xmlns:xlink="http://www.w3.org/1999/xlink"
   xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
   xmlns:oasis="http://docs.oasis-open.org/ns/oasis-exchange/table"
   xmlns:tei="http://www.tei-c.org/ns/1.0"
   xmlns="http://www.tei-c.org/ns/1.0"   
   exclude-result-prefixes="tei xd xs xsi oasis fn" 
   version="3.0">
   
   <xd:desc>
      <xd:p>XSL zur Transformation von MDPI-Journalartikeln von JATS 1.3 und 2.3 (veraltet)</xd:p>
   </xd:desc>
   
   <xsl:output indent="1"/>  
   <!-- Monatsangabe als Wort scheint bei MDPI nicht vorzukommen -->
<!--   
   <xsl:variable name="month">
      <xsl:choose>
         <xsl:when test="//pub-date[1]/month/text()='January'"><xsl:text>01</xsl:text></xsl:when>
         <xsl:when test="//pub-date[1]/month/text()='February'"><xsl:text>02</xsl:text></xsl:when>
         <xsl:when test="//pub-date[1]/month/text()='March'"><xsl:text>03</xsl:text></xsl:when>
         <xsl:when test="//pub-date[1]/month/text()='April'"><xsl:text>04</xsl:text></xsl:when>
         <xsl:when test="//pub-date[1]/month/text()='May'"><xsl:text>05</xsl:text></xsl:when>
         <xsl:when test="//pub-date[1]/month/text()='June'"><xsl:text>06</xsl:text></xsl:when>
         <xsl:when test="//pub-date[1]/month/text()='July'"><xsl:text>07</xsl:text></xsl:when>
         <xsl:when test="//pub-date[1]/month/text()='August'"><xsl:text>08</xsl:text></xsl:when>
         <xsl:when test="//pub-date[1]/month/text()='September'"><xsl:text>09</xsl:text></xsl:when>
         <xsl:when test="//pub-date[1]/month/text()='October'"><xsl:text>10</xsl:text></xsl:when>
         <xsl:when test="//pub-date[1]/month/text()='November'"><xsl:text>11</xsl:text></xsl:when>
         <xsl:when test="//pub-date[1]/month/text()='December'"><xsl:text>12</xsl:text></xsl:when>
         <xsl:otherwise><xsl:value-of select="//pub-date[1]/month"/></xsl:otherwise>
      </xsl:choose>
   </xsl:variable>-->
   
   <xsl:template match="article">
      <TEI><xsl:attribute name="xml:lang"><xsl:value-of select="@xml:lang"/></xsl:attribute>
         <teiHeader>
            <fileDesc>
               <titleStmt>
                  <title><xsl:apply-templates select="//title-group/article-title"></xsl:apply-templates></title>
                  <xsl:for-each select="//contrib[@contrib-type='author']">
                     <author>
                        <xsl:attribute name="ref"><xsl:value-of select="concat('#', generate-id())"/></xsl:attribute>
                        <xsl:if test="xref[@ref-type='corresp']"><xsl:attribute name="role">correspondingAuthor</xsl:attribute></xsl:if>
                        <xsl:call-template name="persName"></xsl:call-template>
                     </author>
                  </xsl:for-each>
                  <xsl:for-each select="//contrib[@contrib-type='editor']">
                     <editor>
                        <xsl:attribute name="ref"><xsl:value-of select="concat('#', generate-id())"/></xsl:attribute>
                        <xsl:call-template name="persName"></xsl:call-template>
                     </editor>
                  </xsl:for-each>
                  <!-- andere Personen: editor, resp... -->                  
               </titleStmt>
               <editionStmt>
                  <edition>Elektronische Ausgabe nach TEI P5</edition>
                  <respStmt>
                     <resp ref="http://id.loc.gov/vocabulary/relators/mrk">Ersteller der TEI-Fassung</resp>
                     <orgName>Universitäts- und Landesbibliothek Darmstadt</orgName>
                  </respStmt>
               </editionStmt>
               <publicationStmt>
                  <distributor ref="http://d-nb.info/gnd/10073682-8">
                     <email>wdm@ulb.tu-darmstadt.de</email>
                     <orgName role="hostingInstitution">Universitäts- und Landesbibliothek Darmstadt</orgName>
                     <idno type="ISIL">http://lobid.org/organisation/DE-17</idno>
                     <idno type="ISNI">http://www.isni.org/0000000110101946</idno>
                     <idno type="GND">http://d-nb.info/gnd/10073682-8</idno>
                     <address>
                        <addrLine>Magdalenenstr. 8, 64289 Darmstadt</addrLine>
                        <country>Germany</country>
                     </address>
                  </distributor>
                  <pubPlace ref="https://d-nb.info/gnd/4011077-1">
                     <placeName>Darmstadt</placeName>
                  </pubPlace>
                  <!-- TODO: idno später eintragen, wenn PURL bekannt -->
                  <!-- <idno type="ULB-intern">https://purl.ulb.tu-darmstadt.de/vj/a000001-0001</idno> -->
                  <availability>
                     <xsl:apply-templates select="//permissions"></xsl:apply-templates>
                    <!-- <xsl:if test="//license//ext-link"><licence><xsl:attribute name="target"><xsl:value-of select="//license//ext-link/@xlink:href"/></xsl:attribute></licence></xsl:if>
                     <p xml:lang="en"><xsl:value-of select="//license/p"/></p>-->
                  </availability>
                  <!-- Datum später eintragen (Datum der Veröffentlichung, nicht der Erstellung der Datei) -->
                  <date when="{format-date(current-date(),'[Y0001]-[M01]-[D01]')}"><xsl:value-of select="year-from-date(current-date())"/></date>  
               </publicationStmt>
               <sourceDesc>
                  <biblStruct>
                     <analytic>
                        <title><xsl:attribute name="level">a</xsl:attribute><xsl:apply-templates select="//title-group/article-title"></xsl:apply-templates></title>
                        <xsl:for-each select="//contrib[@contrib-type='author']">
                           <author>
                              <xsl:attribute name="ref"><xsl:value-of select="concat('#', generate-id())"/></xsl:attribute>
                              <xsl:if test="xref[@ref-type='corresp']"><xsl:attribute name="role">correspondingAuthor</xsl:attribute></xsl:if>
                              <xsl:call-template name="persName"></xsl:call-template>
                           </author>
                        </xsl:for-each>
                        <xsl:for-each select="//contrib[@contrib-type='editor']">
                           <editor>
                              <xsl:attribute name="ref"><xsl:value-of select="concat('#', generate-id())"/></xsl:attribute>
                              <xsl:call-template name="persName"></xsl:call-template>
                           </editor>
                        </xsl:for-each>
                        <xsl:if test="//article-meta/article-id[@pub-id-type='doi']"><idno type="DOI"><xsl:value-of select="//article-meta/article-id[@pub-id-type='doi']"/></idno></xsl:if>
                        <!--<xsl:if test="//self-uri[@xlink:href]"><ref><xsl:attribute name="type">URL</xsl:attribute><xsl:value-of select="//self-uri/@xlink:href"/></ref></xsl:if>--><!-- nicht in MDPI -->
                        <!-- später nachtragen <ptr target="Katalogeintrag"> --> 
                     </analytic>
                     <monogr>
                        <xsl:variable name="journalTitle"><xsl:if test="//journal-title"><xsl:value-of select="//journal-title"/></xsl:if></xsl:variable>
                        <xsl:variable name="volume"><xsl:if test="//article-meta/volume"><xsl:value-of select="concat(', vol. ', //article-meta/volume)"/></xsl:if></xsl:variable>
                        <xsl:variable name="issue"><xsl:if test="//article-meta/issue"><xsl:value-of select="concat(', issue ', //article-meta/issue)"/></xsl:if></xsl:variable>
                        <title level="m"><xsl:value-of select="concat($journalTitle, $volume, $issue)"/></title>
                        <!-- editor? edition? -->
                        <imprint>
                           <publisher><xsl:attribute name="ref"><xsl:value-of select="concat('#', //journal-meta/publisher/publisher-name/generate-id())"/></xsl:attribute><xsl:value-of select="//journal-meta/publisher/publisher-name"/></publisher><!-- mit @ref zur ID des Publishers-->
                           <xsl:choose>
                              <xsl:when test="//journal-meta//publisher-loc">
                                 <pubPlace>
                                    <xsl:attribute name="ref"><xsl:value-of select="concat('#', //journal-meta//publisher-loc/generate-id())"/></xsl:attribute>
                                    <xsl:value-of select="//journal-meta//publisher-loc"/>
                                 </pubPlace>
                              </xsl:when>
                              <xsl:otherwise>
                                 <pubPlace><note>nicht ermittelbar</note></pubPlace>
                              </xsl:otherwise>
                           </xsl:choose>                           
                           <date type="published"><xsl:attribute name="when"><xsl:value-of select="concat(//pub-date[@pub-type='epub']/year, '-', //pub-date[@pub-type='epub']/month, '-', //pub-date[@pub-type='epub']/day)"/></xsl:attribute><xsl:value-of select="//pub-date[@pub-type='epub']/year"/></date>
                           <xsl:if test="//history/date[@date-type='accepted']"><date type="accepted"><xsl:attribute name="when"><xsl:value-of select="concat(//history/date[@date-type='accepted']/year, '-', //history/date[@date-type='accepted']/month, '-', //history/date[@date-type='accepted']/day)"/></xsl:attribute><xsl:value-of select="//history/date[@date-type='accepted']/year"/></date></xsl:if>
                           <biblScope><xsl:attribute name="unit">year</xsl:attribute><xsl:value-of select="//pub-date[@pub-type='epub']/year"/></biblScope>
                           <xsl:if test="//article-meta/volume"><biblScope unit="volume"><xsl:attribute name="n"><xsl:value-of select="//article-meta/volume"/></xsl:attribute><xsl:value-of select="//article-meta/volume"/></biblScope></xsl:if>
                           <xsl:if test="//article-meta/issue"><biblScope unit="issue"><xsl:attribute name="n"><xsl:value-of select="//article-meta/issue"/></xsl:attribute><xsl:value-of select="//article-meta/issue"></xsl:value-of></biblScope></xsl:if>                           
                        </imprint>
                        <!--<xsl:if test="//article-meta/fpage"><biblScope unit="page"><xsl:attribute name="from"><xsl:value-of select="//article-meta/fpage"/></xsl:attribute><xsl:value-of select="//article-meta/fpage"/></biblScope></xsl:if>
                        <xsl:if test="//article-meta/lpage"><biblScope unit="page"><xsl:attribute name="to"><xsl:value-of select="//article-meta/lpage"/></xsl:attribute><xsl:value-of select="//article-meta/lpage"/></biblScope></xsl:if>-->
                        <xsl:choose>
                           <xsl:when test="//article-meta/fpage and //article-meta/lpage">
                              <xsl:variable name="from"><xsl:value-of select="//article-meta/fpage"/></xsl:variable>
                              <xsl:variable name="to"><xsl:value-of select="//article-meta/lpage"/></xsl:variable>
                              <biblScope unit="page" from="{$from}" to="{$to}"><xsl:value-of select="concat('pp. ', $from, '-', $to)"/></biblScope>
                           </xsl:when>
                           <xsl:when test="//article-meta/fpage and not(//article-meta/lpage)">
                              <xsl:variable name="from"><xsl:value-of select="//article-meta/fpage"/></xsl:variable>
                              <biblScope unit="page" from="{$from}"><xsl:value-of select="concat('p. ', $from)"/></biblScope>
                           </xsl:when>
                           <xsl:when test="not(//article-meta/fpage) and //article-meta/lpage">
                              <xsl:variable name="to"><xsl:value-of select="//article-meta/lpage"/></xsl:variable>
                              <biblScope unit="page" to="{$to}"><xsl:value-of select="concat('p. ', $to)"/></biblScope>
                           </xsl:when>
                           <xsl:otherwise></xsl:otherwise>
                        </xsl:choose>
                        <xsl:if test="//article-meta/elocation-id"><biblScope unit="elocation-id"><xsl:value-of select="//article-meta/elocation-id"/></biblScope></xsl:if>
                     </monogr>
                     <series>
                        <title level="j" type="main"><xsl:value-of select="//journal-title"/></title>
                        <xsl:for-each select="//abbrev-journal-title"><title level="j" type="abbr"><xsl:attribute name="subtype"><xsl:value-of select="@abbrev-type"/></xsl:attribute><xsl:value-of select="."/></title></xsl:for-each>
                        <idno type="ISSN"><xsl:value-of select="//issn[@pub-type='epub']"/></idno>
                       <!-- <ref target="{katalog-URL}">Katalogisat</ref>
                        <idno type="ZBN"></idno><!-\- s. Issue #18 -\->-->
                     </series>
                  </biblStruct>
               </sourceDesc>               
            </fileDesc>
            <profileDesc>
               <langUsage>                  
                  <language ident="{@xml:lang}"><xsl:value-of select="@xml:lang"/></language><!-- evtl. überarbeiten wg. Sprachbezeichnung im text und wg. mehreren Sprachen -->
               </langUsage>
               <!--<textClass>
                  <classCode></classCode>
               </textClass>-->
            </profileDesc>
            <encodingDesc>
               <p xml:lang="de">Automatische Konversion der digitalen Vorlage mittels</p>
               <p xml:lang="en">The digital source has been converted using</p>
               <p><ref target="NN">Workflow Digitale Medien @ ULB Darmstadt</ref></p>
            </encodingDesc>
            <revisionDesc status="embargoed">
           <!--    <change status="created" when="2020-05-21T13:59:00Z" who="#ed"/>-->
               <change status="automaticConversion" who="#toolbox"><xsl:attribute name="when"><xsl:value-of select="format-date(current-date(),'[Y0001]-[M01]-[D01]')"/></xsl:attribute></change>
              <!-- <change status="published" when="2023-03-31" />-->
            </revisionDesc>
         </teiHeader>
         <standOff type="metaData">
            <xsl:if test="//contrib/name">
               <listPerson source="JATS">
                  <xsl:apply-templates select="//contrib"/>
               </listPerson>
            </xsl:if>
            <!-- institution, aff, publisher; bei MDPI kein institution -->
            <xsl:if test="//publisher/publisher-name | //article-meta/aff | //contrib/collab">
               <listOrg source="JATS">
                 <!-- <xsl:apply-templates select="orgs"/> -->
                  <xsl:call-template name="orgs"/>
               </listOrg>
            </xsl:if>
            <!-- listPlace eigentlich nur publisher-loc -->
            <xsl:if test="//publisher-loc[not(ancestor::ref)]">
               <listPlace source="JATS">
                  <xsl:apply-templates select="//publisher-loc[not(ancestor::ref)]"></xsl:apply-templates>
               </listPlace>
            </xsl:if>
            <xsl:if test="//ref-list">
               <listBibl>
                  <xsl:call-template name="listBibl"/>
               </listBibl>
            </xsl:if>
           <!-- <listAnnotation></listAnnotation>-->
         </standOff>
         <!--<standOff type="contentData"/>-->
         <text>
            <xsl:if test="//kwd-group | //abstract | //funding-group">
               <xsl:apply-templates select="//front"/>               
            </xsl:if>           
            <body>
               <xsl:apply-templates select="//body"/>
            </body>
            <xsl:if test="//back">
               <back>
                  <xsl:apply-templates select="//back"/>
               </back>
            </xsl:if>
         </text>
      </TEI>
   </xsl:template>
   
   <xd:desc>
      <xd:p>standOff</xd:p>
   </xd:desc>
   <xsl:template match="contrib">
      <xsl:variable name="rid">
         <xsl:choose>
        <!--    <xsl:when test="xref[@ref-type='aff']"><xsl:value-of select="xref[@ref-type='aff']/@rid" separator="|"/></xsl:when>-->
            <xsl:when test="xref/@rid">
        <!--       <xsl:for-each select="xref">-->
                  <xsl:value-of select="xref/@rid" separator="@"/>
               <!--</xsl:for-each>-->
            </xsl:when>
            <xsl:when test="@rid">
               <xsl:value-of select="@rid"/>
            </xsl:when>
      <!--      <xsl:otherwise/>-->
         </xsl:choose>
      </xsl:variable>
      <xsl:variable name="corresp"><xsl:value-of select="xref[@ref-type='corresp']/@rid"/></xsl:variable>
      <person>
         <xsl:attribute name="xml:id"><xsl:value-of select="generate-id()"/></xsl:attribute>
         <persName>
            <surname><xsl:value-of select="name/surname"></xsl:value-of></surname>
            <forename><xsl:value-of select="name/given-names"></xsl:value-of></forename>
            <xsl:if test="name/suffix"><addName><xsl:value-of select="name/suffix"/></addName></xsl:if>
            <!-- email entfernt! -->
<!--            <xsl:choose>
               <xsl:when test="email"><email><xsl:value-of select="email"/></email></xsl:when>
               <xsl:when test="$corresp = //author-notes/corresp/@id"><email><xsl:value-of select="//author-notes/corresp[$corresp = @id]/email"></xsl:value-of></email></xsl:when>
               <xsl:when test="//aff/@id = $rid and //aff/email"><email><xsl:value-of select="//aff[@id = $rid]/email"/></email></xsl:when>
               <xsl:otherwise/>
            </xsl:choose>-->
         </persName>
       <!--  <xsl:choose>-->
<!--            <xsl:when test="//aff[@id = $rid and institution]"><affiliation><orgName><xsl:attribute name="ref"><xsl:value-of select="concat('#',//aff[@id = $rid]/institution/generate-id())"/></xsl:attribute><xsl:value-of select="//aff[@id = $rid]/institution/text()"/></orgName></affiliation></xsl:when>
            <!-\- folgende when evtl. nicht bei MDPI; TODO checken, dann evtl. entfernen für MDPI-Version -\->
            <xsl:when test="//aff[@id = $rid and institution-wrap]"><affiliation><xsl:for-each select="//aff[@id = $rid]/institution-wrap/institution"><orgName><xsl:attribute name="ref"><xsl:value-of select="concat('#', ./generate-id())"/></xsl:attribute><xsl:value-of select="text()"/></orgName></xsl:for-each></affiliation></xsl:when>-->
         <xsl:for-each select="//article-meta/aff">
            <xsl:if test="tokenize($rid, '@') = @id">
               <affiliation><!--<xsl:for-each select="//aff[@id = $rid]">-->
                  <orgName><xsl:attribute name="ref">
                     <xsl:value-of select="concat('#', generate-id())"/>
                  </xsl:attribute><xsl:value-of select="text()"/>
                  </orgName><!--</xsl:for-each>-->
               </affiliation>
            </xsl:if>
         </xsl:for-each>
          
         <!--</xsl:choose>-->
         <xsl:if test="contrib-id[@contrib-id-type='orcid']">
            <idno type="ORCID">
               <xsl:value-of select="contrib-id[@contrib-id-type='orcid']"/>
            </idno>
         </xsl:if>
      </person>
   </xsl:template>
   
   <xsl:template name="persName">
      <xsl:choose>
         <xsl:when test="collab">
            <orgName><xsl:apply-templates select="collab"/></orgName>
         </xsl:when>
         <xsl:otherwise>
            <persName>
               <xsl:choose>
                  <xsl:when test="name/suffix">
                     <xsl:value-of select="concat(name/surname, ', ', name/given-names, ', ', name/suffix)"/>
                  </xsl:when>
                  <xsl:otherwise><xsl:value-of select="concat(name/surname, ', ', name/given-names)"/></xsl:otherwise>
               </xsl:choose>
            </persName>
         </xsl:otherwise>
      </xsl:choose>      
   </xsl:template>
   
   <xsl:template name="orgs">
      <!--<xsl:for-each select="//publisher/publisher-name | //institution | //aff[not(institution) and not(institution-wrap)]">-->
      <xsl:for-each select="//publisher/publisher-name | //article-meta/aff | //contrib/collab">
         <org>
            <xsl:attribute name="xml:id"><xsl:value-of select="generate-id()"/></xsl:attribute>
            <orgName><xsl:value-of select="text()"/></orgName>
         </org>
      </xsl:for-each>
   </xsl:template>
   
<!--   <xsl:template match="aff/label"/>-->
   
   <!-- soll email wirklich weggelassen werden? TODO noch mal überdenken -->
<!--   <xsl:template match="email">
      <email><xsl:apply-templates/></email>
   </xsl:template>-->
   
   <xsl:template match="publisher-loc[not(ancestor::ref)]">
      <place>
         <xsl:attribute name="xml:id"><xsl:value-of select="generate-id()"/></xsl:attribute>
         <placeName><xsl:value-of select="."/></placeName>
      </place>
   </xsl:template>

   <xd:desc>
      <xd:p>front</xd:p>
   </xd:desc>   
   <xsl:template match="front">
      <front>
         <xsl:apply-templates select="//abstract"/>
         <xsl:apply-templates select="//kwd-group"/>
     <!--    <xsl:apply-templates select="//funding-group"/>-->
      </front>
   </xsl:template>
   
   <xsl:template match="abstract">
      <xsl:if test="title"/>
      <div type="abstract" xml:id="abstract"><!-- xml:lang="en" -->
         <head>Abstract</head>
         <xsl:apply-templates/>
      </div>
   </xsl:template>
   
   <!-- TODO term statt list/item, bzw. label/term? -->
   <xsl:template match="kwd-group">
      <div type="keywords">
       <!--  <head>Keywords</head>-->
         <list>
            <xsl:apply-templates/>
         </list>
      </div>
   </xsl:template> 
   
   <xsl:template match="kwd">
      <item><xsl:apply-templates/></item>
   </xsl:template>
   
   <xsl:template match="kwd-group/title">
      <head><xsl:apply-templates/></head>
   </xsl:template>
<!--   
   <xsl:template match="funding-group">
      <div type="funding"><xsl:apply-templates/></div>
   </xsl:template>
   
   <xsl:template match="award-group">
      <div type="award"><note><xsl:apply-templates/></note></div>
   </xsl:template>
   
   <xsl:template match="funding-source">
      <orgName><xsl:apply-templates/></orgName>
   </xsl:template>
   
   <xsl:template match="award-id">
      <idno type="award-id"><xsl:apply-templates/></idno>
   </xsl:template>
   
   <xsl:template match="funding-statement">
      <note type="funding-statement"><xsl:apply-templates/></note>
   </xsl:template>
   -->
   <xd:desc>
      <xd:p>Inhalt</xd:p>
   </xd:desc>
   <xsl:template match="//body">
      <xsl:apply-templates></xsl:apply-templates>
   </xsl:template>
   
   <xsl:template match="title">
      <head>
         <xsl:if test="preceding-sibling::label"><xsl:attribute name="n"><xsl:value-of select="preceding-sibling::label"/></xsl:attribute></xsl:if>
         <xsl:apply-templates/>
      </head>
   </xsl:template>
   
   <xsl:template match="sec/label"/>
   
   <xsl:template match="p">
      <p><xsl:apply-templates></xsl:apply-templates></p>
   </xsl:template>
   
   <xsl:template match="sec">
      <div>
         <xsl:if test="@id"><xsl:attribute name="xml:id"><xsl:value-of select="translate(@id, ' ', '_')"/></xsl:attribute></xsl:if>
         <xsl:if test="@sec-type"><xsl:attribute name="type"><xsl:value-of select="@sec-type"/></xsl:attribute></xsl:if>
         <xsl:apply-templates></xsl:apply-templates>
      </div>
   </xsl:template>
   
   <xsl:template match="disp-quote">
      <quote><xsl:apply-templates/></quote>
   </xsl:template>
   
   <xsl:template match="attrib">
      <note type="attribution"><xsl:apply-templates/></note>
   </xsl:template>
   
   <xsl:template match="xref">
      <ref>
         <xsl:attribute name="target"><xsl:value-of select="concat('#', @rid)"/></xsl:attribute>
         <xsl:if test="@ref-type"><xsl:attribute name="type"><xsl:value-of select="@ref-type"/></xsl:attribute></xsl:if>
         <xsl:apply-templates/>
      </ref>
   </xsl:template>
   
   <xsl:template match="ext-link">
      <ref>
         <xsl:attribute name="target"><xsl:value-of select="@xlink:href"/></xsl:attribute><xsl:apply-templates/>
      </ref>
   </xsl:template>
   
   <xsl:template match="uri">
      <idno type="URI"><xsl:apply-templates/></idno>
   </xsl:template>
   
   <xsl:template match="note">
      <note><xsl:apply-templates/></note>
   </xsl:template>
   
   <xsl:template match="named-content">
      <seg type="named-content"><xsl:apply-templates/></seg>
   </xsl:template>
   
   <xsl:template match="statement">
      <label type="statement">
         <xsl:apply-templates select="label"/>
            <seg>
               <xsl:apply-templates select="p"/>
            </seg>     
      </label>
   </xsl:template>
   
   <xsl:template match="statement/label">
      <xsl:apply-templates/>
   </xsl:template>
   
   <xsl:template match="statement/p">
      <xsl:apply-templates/>
   </xsl:template>
      
      
   <xd:desc>
      <xd:p>Back</xd:p>
   </xd:desc>
   <xsl:template match="back">
      <xsl:if test=" //article-title/xref">
         <div type="fn">
            <p><xsl:value-of select="//fn[@id=//article-title/xref/@rid]/p"/></p>
         </div>
      </xsl:if>         
      <xsl:apply-templates/>
   </xsl:template>   
   
   <!-- TODO wenn mit ID in listAnnotation -->
   <xsl:template match="fn-group">
      <note type="group"><xsl:apply-templates/></note> 
   </xsl:template>
   
   <xsl:template match="fn">
      <note><xsl:apply-templates/></note> 
   </xsl:template>
   
   <xsl:template match="fn/label">
      <label><xsl:apply-templates/></label> 
   </xsl:template>
   
   <xsl:template match="ack">
      <div xml:id="acknowledgement">
         <xsl:apply-templates/>
      </div>      
   </xsl:template>
   
   <xsl:template match="notes">
      <div type="notes">
         <xsl:apply-templates></xsl:apply-templates>
      </div>
   </xsl:template>
   
   <xsl:template match="app-group">
      <div type="appendix"><xsl:apply-templates/></div>
   </xsl:template>
   
   <xsl:template match="app">
      <div>
         <xsl:if test="@id"><xsl:attribute name="xml:id"><xsl:value-of select="@id"/></xsl:attribute></xsl:if>
         <xsl:apply-templates/>
      </div>
   </xsl:template>
   
   <!-- bei MDPI ist supplementary-material ein leeres Element, bei anderen Verlagen Kinderelemente und andere Benutzung möglich; ab, da ptr nicht alleine stehen kann -->
   <xsl:template match="supplementary-material">
      <ab><ptr><xsl:attribute name="xml:id"><xsl:value-of select="@id"/></xsl:attribute><xsl:attribute name="target"><xsl:value-of select="@xlink:href"/></xsl:attribute></ptr></ab>
   </xsl:template>
   
   <xsl:template match="glossary">
      <div type="gloss"><xsl:apply-templates/></div>
   </xsl:template>
   
   <!-- Anm. gloss-group ist veraltet-->
   <xsl:template match="gloss-group">
      <div><xsl:apply-templates/></div>
   </xsl:template>
   
   <xsl:template match="def-list">
      <list><xsl:apply-templates/></list>
   </xsl:template>
   
<!--   <xsl:template match="def-item">
      
   </xsl:template>-->
   
   <xsl:template match="def-item/term">
      <label><term><xsl:apply-templates/></term></label>
   </xsl:template>
   
   <xsl:template match="def-item/def/p">
      <item><gloss><xsl:apply-templates/></gloss></item>
   </xsl:template>
   
   <xsl:template match="bio">
      <div type="bio"><xsl:apply-templates/></div>
   </xsl:template>
   
   <xd:desc>
      <xd:p>Bibliographie</xd:p>
   </xd:desc>
   
   <xsl:template match="back/ref-list"/>      
   
   <xsl:template match="ref-list" name="listBibl">
      <xsl:apply-templates select="//ref"></xsl:apply-templates>
   </xsl:template>
   
   <xsl:template match="ref">
      <bibl>
         <xsl:if test="child::node()[@publication-type]">
            <xsl:attribute name="type"><xsl:value-of select="child::node()/@publication-type"/></xsl:attribute>
         </xsl:if>
         <xsl:attribute name="n"><xsl:value-of select="./label"/></xsl:attribute>
<!--         <xsl:choose>
            <xsl:when test="mixed-citation[not(article-title)]">
               <xsl:apply-templates select="mixed-citation"/>
            </xsl:when>
            <xsl:when test="element-citation[not(article-title)]">
               <xsl:attribute name="xml:id"><xsl:value-of select="parent::ref/@id"/></xsl:attribute>
               <xsl:apply-templates select="mixed-citation"/>
            </xsl:when>
            <xsl:when test="citation[not(article-title)]">
               <xsl:apply-templates select="citation"/>
            </xsl:when>
            <xsl:otherwise><xsl:apply-templates/></xsl:otherwise>
         </xsl:choose>-->
         <xsl:attribute name="xml:id"><xsl:value-of select="@id"/></xsl:attribute>
         <xsl:apply-templates/>
      </bibl>
   </xsl:template>
   
<!--   <xsl:template match="mixed-citation">     
         <xsl:attribute name="xml:id"><xsl:value-of select="parent::ref/@id"/></xsl:attribute>
         <xsl:apply-templates/>
   </xsl:template>
   
   <xsl:template match="element-citation">       
         <xsl:apply-templates/>
   </xsl:template>
   
   <!-\- Anm. citation ist veraltet -\->
   <xsl:template match="citation">    
         <xsl:attribute name="xml:id"><xsl:value-of select="parent::ref/@id"/></xsl:attribute>
         <xsl:apply-templates/>
   </xsl:template>
   -->
   <xsl:template match="ref//article-title">
      <title level="a"><xsl:apply-templates/></title>
   </xsl:template>
   
   <xsl:template match="ref//source">
      <title level="j"><xsl:apply-templates/></title>
   </xsl:template>
   
   <xsl:template match="ref//year">
      <date>
         <xsl:choose>
            <xsl:when test="contains(text(), '&#x2013;')">
               <xsl:attribute name="from"><xsl:value-of select="substring-before(text(), '&#x2013;')"/></xsl:attribute>
               <xsl:attribute name="to"><xsl:value-of select="substring-after(text(), '&#x2013;')"/></xsl:attribute>
               <xsl:apply-templates/>
            </xsl:when>
            <xsl:when test="contains(text(), '-')">
               <xsl:attribute name="from"><xsl:value-of select="substring-before(text(), '-')"/></xsl:attribute>
               <xsl:attribute name="to"><xsl:value-of select="substring-after(text(), '-')"/></xsl:attribute>
               <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
               <xsl:attribute name="when"><xsl:value-of select="text()"/></xsl:attribute><xsl:apply-templates/>
            </xsl:otherwise>
         </xsl:choose>
         
      </date>
   </xsl:template>
   
   <!-- TODO: muss noch entschieden werden: month und day bei Datumsangaben (kommt selten vor) -->
   <xsl:template match="ref//month"/>
   
   <xsl:template match="ref//day"/>
   
   <xsl:template match="ref//volume">
      <biblScope unit="volume"><xsl:attribute name="n"><xsl:value-of select="text()"/></xsl:attribute><xsl:apply-templates/></biblScope>
   </xsl:template>
   
   <xsl:template match="fpage">
      <xsl:variable name="from"><xsl:value-of select="."/></xsl:variable>
      <xsl:if test="following-sibling::lpage">
         <xsl:variable name="to"><xsl:value-of select="following-sibling::lpage"/></xsl:variable>
         <biblScope unit="page" from="{$from}" to="{$to}"><xsl:value-of select="concat('pp. ', $from, '-', $to)"/></biblScope>
      </xsl:if>
      <xsl:if test="not(following-sibling::lpage)">
         <biblScope unit="page" from="{$from}"><xsl:value-of select="concat('p. ', $from)"/></biblScope>
      </xsl:if>
   </xsl:template>
   
   <xsl:template match="lpage">
      <xsl:variable name="to"><xsl:value-of select="."/></xsl:variable>
      <xsl:if test="not(preceding-sibling::fpage)">
         <biblScope unit="page" to="{$to}"><xsl:value-of select="concat('p. ', $to)"/></biblScope>
      </xsl:if>      
   </xsl:template>
   
   <xsl:template match="size">
      <extent><xsl:apply-templates/></extent>
   </xsl:template>
   
   <xsl:template match="elocation-id">
      <biblScope unit="elocation-id"><xsl:apply-templates/></biblScope>
   </xsl:template>
   
   <xsl:template match="pub-id">
      <idno>
         <xsl:choose>
            <xsl:when test="@pub-id-type='doi'"><xsl:attribute name="type">DOI</xsl:attribute></xsl:when>
            <xsl:when test="@pub-id-type='pmid'"><xsl:attribute name="type">PMID</xsl:attribute></xsl:when>
            <xsl:when test="@pub-id-type='other'"><xsl:attribute name="type">other</xsl:attribute></xsl:when>
            <xsl:otherwise><xsl:attribute name="type">unknown</xsl:attribute></xsl:otherwise>
         </xsl:choose>         
         <xsl:value-of select="."/>
      </idno>
   </xsl:template>
   
   <xsl:template match="ref//publisher-loc">
      <pubPlace><xsl:apply-templates/></pubPlace>
   </xsl:template>
   
   <xsl:template match="publisher-name">
      <publisher><xsl:apply-templates/></publisher>
   </xsl:template>
   
   <xsl:template match="edition">
      <edition><xsl:apply-templates/></edition>
   </xsl:template>
   
   <xsl:template match="person-group[@person-group-type='author']/name">
      <author><xsl:apply-templates></xsl:apply-templates></author>
   </xsl:template>
   
   <xsl:template match="person-group[@person-group-type='editor']/name">
      <editor><xsl:apply-templates></xsl:apply-templates></editor>
   </xsl:template>
   
   <xsl:template match="person-group[@person-group-type='author']/collab">
      <author><orgName><xsl:apply-templates/></orgName></author>
   </xsl:template>
   
   <xsl:template match="person-group[@person-group-type='editor']/collab">
      <editor><orgName><xsl:apply-templates/></orgName></editor>
   </xsl:template>
   
   <xsl:template match="surname">
      <surname><xsl:apply-templates/></surname>
   </xsl:template>
   
   <xsl:template match="given-names">
      <forename><xsl:apply-templates/></forename>
   </xsl:template>
   
   <xsl:template match="suffix">
      <addName><xsl:apply-templates/></addName>
   </xsl:template>
   
   <xsl:template match="ref/label"/>
   
   <xsl:template match="ref//comment">
      <note><xsl:apply-templates/></note>
   </xsl:template>
   
   <!-- teilw. werden government reports (gov) im JATS auch als comment (wird zu note) getaggt, desw. hier mangels TEI-Äquivalent auch als note -->
   <xsl:template match="gov">
      <note><xsl:apply-templates/></note>
   </xsl:template>
   
   <xsl:template match="date-in-citation">
      <date>
         <xsl:choose>
            <xsl:when test="@content-type='access-date'"><xsl:attribute name="type"><xsl:text>accessed</xsl:text></xsl:attribute></xsl:when>
            <xsl:otherwise><xsl:attribute name="type"><xsl:value-of select="@content-type"/></xsl:attribute></xsl:otherwise><!-- Daten beobachten und evtl. rausnehmen -->
         </xsl:choose>
         <xsl:if test="@content-type"></xsl:if>
         <xsl:if test="@iso-8601-date"><xsl:attribute name="when"><xsl:value-of select="@iso-8601-date"/></xsl:attribute></xsl:if>
         <xsl:apply-templates/>
      </date>
   </xsl:template>
   
   <xsl:template match="issn">
      <idno type="ISSN"><xsl:apply-templates/></idno>
   </xsl:template>
   
   <xsl:template match="isbn">
      <idno type="ISBN"><xsl:apply-templates/></idno>
   </xsl:template>
   
   <xsl:template match="patent">
      <idno type="patent"><xsl:apply-templates/></idno>
   </xsl:template>
   
   <xsl:template match="conf-loc">
      <placeName type="conf-loc"><xsl:apply-templates/></placeName>
   </xsl:template>
   
   <!-- Datum anpassen? -->
   <xsl:template match="conf-date">
      <date type="conf-date"><xsl:apply-templates/></date>
   </xsl:template>
   
   <xsl:template match="supplement">
      <note type="supplement"><xsl:apply-templates/></note>
   </xsl:template>
   
   <xsl:template match="std">
      <idno><xsl:apply-templates/></idno>
   </xsl:template>
   
   <xsl:template match="series">
      <title level="s"><xsl:apply-templates/></title>
   </xsl:template>
   
   <xsl:template match="ref//issue">
      <biblScope unit="issue"><xsl:attribute name="n"><xsl:value-of select="text()"/></xsl:attribute><xsl:apply-templates/></biblScope>
   </xsl:template>
   
   <xsl:template match="ref//issue-part">
      <biblScope unit="issue-part"><xsl:attribute name="n"><xsl:value-of select="text()"/></xsl:attribute><xsl:apply-templates/></biblScope>
   </xsl:template>
   
   <xsl:template match="ref//page-range">
      <citedRange unit="page"><xsl:apply-templates/></citedRange>
   </xsl:template>
   
   <xsl:template match="ref//aff">
      <affiliation><xsl:apply-templates/></affiliation>
   </xsl:template>
   
   <!-- aus Version 2.3; veraltet -->
   <xsl:template match="access-date">
      <date><xsl:apply-templates/></date>
   </xsl:template>
   
   <xd:desc>
      <xd:p>mathematische Formel</xd:p>
   </xd:desc>
   <xsl:template match="inline-formula">
      <formula><xsl:attribute name="rend">
         <xsl:text>inline</xsl:text>
      </xsl:attribute>
         <xsl:if test="@id">
            <xsl:attribute name="xml:id">
               <xsl:value-of select="@id"/>
            </xsl:attribute></xsl:if>
         <xsl:copy-of select="node()"/>
      </formula>
   </xsl:template>
   
   <xsl:template match="disp-formula">
      <xsl:choose>
         <xsl:when test="parent::p">
            <xsl:call-template name="disp-formula"/>
         </xsl:when>
         <xsl:otherwise>
            <p><xsl:call-template name="disp-formula"/></p>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   
   <xsl:template name="disp-formula">
      <formula>
         <xsl:attribute name="rend">
            <xsl:text>block</xsl:text>
         </xsl:attribute>
         <xsl:attribute name="n">
            <xsl:value-of select="label"/>            
         </xsl:attribute>
         <xsl:if test="@id">
            <xsl:attribute name="xml:id">
               <xsl:value-of select="translate(@id, ' ', '_')"/>
            </xsl:attribute>
         </xsl:if>
         <!-- <xsl:call-template name="math"/>-->
         <xsl:apply-templates/>
      </formula>
   </xsl:template>
   
   <xsl:template match="mml:math">
      <xsl:copy-of select="."></xsl:copy-of>
   </xsl:template>
   
   <xsl:template match="disp-formula/label"/>
   
   
   <xd:desc>
      <xd:p>Graphiken</xd:p>
   </xd:desc>
   <xsl:template match="fig">
      <figure>
         <xsl:if test="@id">
            <xsl:attribute name="xml:id"><xsl:value-of select="translate(@id, ' ', '_')"/></xsl:attribute>
         </xsl:if>
         <xsl:apply-templates/>
      </figure>
   </xsl:template>
   
   <xsl:template match="fig/label">
      <head><xsl:apply-templates/></head>
   </xsl:template>
   
   <xsl:template match="graphic | inline-graphic">
      <xsl:choose>
         <xsl:when test="parent::sec"><p><graphic><xsl:attribute name="url"><xsl:value-of select="@xlink:href"/></xsl:attribute></graphic></p></xsl:when>
         <xsl:otherwise><graphic><xsl:attribute name="url"><xsl:value-of select="@xlink:href"/></xsl:attribute></graphic></xsl:otherwise>
      </xsl:choose>
      
   </xsl:template>
   
   <xsl:template match="caption/p">
      <label><xsl:apply-templates/></label>
   </xsl:template>
   
   <xd:desc>
      <xd:p>Tabellen</xd:p>
   </xd:desc>
   <xsl:template match="table-wrap">
      <figure><xsl:attribute name="xml:id"><xsl:value-of select="translate(@id, ' ', '_')"/></xsl:attribute><xsl:apply-templates/></figure>
   </xsl:template>

   <xsl:template match="object-id"/>  
   
   <xsl:template match="table-wrap/label">
      <head>
         <xsl:apply-templates/>
         <xsl:for-each select="preceding-sibling::object-id | following-sibling::object-id">
            <idno><xsl:attribute name="type"><xsl:value-of select="@pub-id-type"/></xsl:attribute><xsl:apply-templates/></idno>
         </xsl:for-each>
      </head>
   </xsl:template>  

   <xsl:template match="table-wrap-foot/fn">
      <note><xsl:apply-templates/></note>
   </xsl:template>

   <xsl:template match="table">
      <table><xsl:apply-templates/></table>
   </xsl:template>
   
   <xsl:template match="array">
      <table><xsl:apply-templates/></table>
   </xsl:template>
   
   <xsl:template match="thead/tr">
      <row role="label"><xsl:apply-templates/></row>
   </xsl:template>
   
   <xsl:template match="tbody/tr">
      <row role="data"><xsl:apply-templates/></row>      
   </xsl:template>
   
   <xsl:template match="th">
      <cell role="label">
         <xsl:if test="@rowspan"><xsl:attribute name="rows"><xsl:value-of select="@rowspan"/></xsl:attribute></xsl:if>
         <xsl:if test="@colspan"><xsl:attribute name="cols"><xsl:value-of select="@colspan"/></xsl:attribute></xsl:if>
         <xsl:apply-templates/>
      </cell>
   </xsl:template>
   
   <xsl:template match="td">
      <cell role="data">
         <xsl:if test="@rowspan"><xsl:attribute name="rows"><xsl:value-of select="@rowspan"/></xsl:attribute></xsl:if>
         <xsl:if test="@colspan"><xsl:attribute name="cols"><xsl:value-of select="@colspan"/></xsl:attribute></xsl:if>
         <xsl:apply-templates/>
      </cell>
   </xsl:template>
   
   <xsl:template match="table-wrap-group">
      <div><xsl:apply-templates/></div>
   </xsl:template>
      
   <xsl:template match="table-wrap-group/label">
      <head><xsl:apply-templates/></head>
   </xsl:template>
   
   <xd:desc>
      <xd:p>Listen</xd:p>
   </xd:desc>
   <xsl:template match="list">
      <list><xsl:apply-templates/></list>
   </xsl:template>
   
   <xsl:template match="list-item">
      <item><xsl:apply-templates/></item>
   </xsl:template>
   
   <xsl:template match="list-item/label">
      <label><xsl:apply-templates/></label>
   </xsl:template>
   
    <xd:desc>
       <xd:p>Styles: italic etc.</xd:p>
    </xd:desc>
   <xsl:template match="italic">
      <hi rend="italic"><xsl:apply-templates></xsl:apply-templates></hi>
   </xsl:template>
   
   <xsl:template match="bold">
      <hi rend="bold"><xsl:apply-templates></xsl:apply-templates></hi>
   </xsl:template>
   
   <xsl:template match="sup">
      <hi rend="sup"><xsl:apply-templates/></hi>
   </xsl:template>
   
   <xsl:template match="sub">
      <hi rend="sub"><xsl:apply-templates/></hi>
   </xsl:template>
   
   <xsl:template match="monospace">
      <hi rend="monospace"><xsl:apply-templates/></hi>
   </xsl:template>
   
   <xsl:template match="underline">
      <hi rend="underline"><xsl:apply-templates/></hi>
   </xsl:template>
   
   <xsl:template match="overline">
      <hi rend="overline"><xsl:apply-templates/></hi>
   </xsl:template>   
   
   <xsl:template match="sc">
      <hi rend="smallCaps"><xsl:apply-templates/></hi>
   </xsl:template>
   
   <xsl:template match="sans-serif">
      <hi rend="sansSerif"><xsl:apply-templates/></hi>
   </xsl:template>
   
   <xsl:template match="strike">
      <hi rend="strike"><xsl:apply-templates/></hi>
   </xsl:template>
   
   <xsl:template match="preformat">
      <ab type="pre"><xsl:apply-templates/></ab>
   </xsl:template>
   
   <xd:desc>
      <xd:p>Permissions und Lizenz</xd:p>
   </xd:desc>
   <xsl:template match="permissions">
      <xsl:apply-templates/>
   </xsl:template>
   
   <xsl:template match="copyright-statement"/>   
   
   <xsl:template match="copyright-year"/>
   
   <xsl:template match="license">
      <licence>
         <xsl:if test="descendant::ext-link"><xsl:attribute name="target"><xsl:value-of select="descendant::ext-link/@xlink:href"/></xsl:attribute></xsl:if>
         <xsl:if test="//parent::permissions/child::copyright-year"><xsl:attribute name="when"><xsl:value-of select="//parent::permissions/child::copyright-year"/></xsl:attribute></xsl:if>
         <xsl:if test="//@license-type"><xsl:attribute name="n"><xsl:value-of select="//@license-type"/></xsl:attribute></xsl:if>
         <xsl:if test="//parent::permissions/child::copyright-statement"><label><xsl:value-of select="//parent::permissions/child::copyright-statement"></xsl:value-of></label></xsl:if>
         <xsl:apply-templates/>
      </licence>
   </xsl:template>
   
   <xsl:template match="license/p">
      <p><xsl:apply-templates/></p>
   </xsl:template>
   
   <xsl:template match="license-p">
      <p><xsl:apply-templates/></p>
   </xsl:template>   

</xsl:stylesheet> 
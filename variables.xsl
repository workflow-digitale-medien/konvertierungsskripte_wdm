<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns="http://www.tei-c.org/ns/1.0"
  exclude-result-prefixes="#all"
  version="3.0">
  
  <!-- orgName for ULB -->
  <xsl:variable name="ULBOrgName">
    <orgName ref="http://d-nb.info/gnd/10073682-8">Universitäts- und Landesbibliothek Darmstadt</orgName>
  </xsl:variable>
  
  <!-- editionStmt for texts that have been reprocessed by ULB -->
  <xsl:variable name="ULBEditionStmt">
      <editionStmt xml:space="preserve">
        <edition>Automatically reprocessed version of an original file</edition>
        <respStmt>
          <resp>processed by</resp>
          <xsl:sequence select="$ULBOrgName" />
        </respStmt>
      </editionStmt>
      </xsl:variable>
  
  <!-- publicationStmt for texts that have been reprocessed by ULB -->
  <xsl:variable name="ULBPublisher" xml:space="preserve"
    ><publisher ref="http://d-nb.info/gnd/10073682-8">
          <orgName>Universitäts- und Landesbibliothek Darmstadt</orgName>
        </publisher>
        <pubPlace>Darmstadt</pubPlace>
        <date when="{substring(string(current-date()), 1, 10)}"
      /></xsl:variable>
  
  <xsl:variable name="ULBdistributor" as="node()*">
    <distributor xmlns="http://www.tei-c.org/ns/1.0">
      <orgName role="hostingInstitution">Universitäts- und Landesbibliothek Darmstadt</orgName>
      <idno type="ISIL">http://lobid.org/organisation/DE-17</idno>
      <idno type="ISNI">http://www.isni.org/0000000110101946</idno>
      <idno type="GND">http://d-nb.info/gnd/10073682-8</idno>
      <address>
        <addrLine>Magdalenenstr. 8, 64289 Darmstadt</addrLine>
        <country>Germany</country>
      </address>
    </distributor>
  </xsl:variable>
  
  <xsl:variable name="ULBextent">
    <editionStmt xmlns="http://www.tei-c.org/ns/1.0">
      <edition>Elektronische Ausgabe nach TEI P5</edition>
    </editionStmt>
  </xsl:variable>
  
  <!-- Link zur Dokumentation muss noch eingefügt werden -->
  <xsl:variable name="ULBprojectDesc">
    <projectDesc>
      <p>Text converted to <ref target="docu_DaBF"><choice>
        <abbr>DaBF</abbr>
        <expan>Darmstädter Basisformat</expan>
      </choice></ref> of the University and States Library Darmstadt</p>
    </projectDesc>
  </xsl:variable>
  
  <xsl:variable name="ULB_schemaRef">
    <schemaRef type="DaBF_ODD" url="http://persistent-uri.de/DaBF.odd"/>
  </xsl:variable>
  
</xsl:stylesheet>
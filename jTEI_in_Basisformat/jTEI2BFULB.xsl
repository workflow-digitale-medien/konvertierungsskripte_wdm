<?xml version="1.0" encoding="UTF-8"?>
<!--
     author:JF Jens Freund <jens.freund@tu-darmstadt.de>
     author:DK Dario Kampkaspar <dario.kampkaspar@tu-darmstadt.de
     author:TS Thomas Stäcker <thomas.staecker@tu-darmstadt.de 
-->
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema"
  xmlns:tei="http://www.tei-c.org/ns/1.0"
  xmlns:mets="http://www.loc.gov/METS/"
  xmlns:dcterms="http://purl.org/dc/terms/"
  xmlns="http://www.tei-c.org/ns/1.0"
  exclude-result-prefixes="#all"
  version="3.0">
  
  <!-- allow passing the meta data as parameter so we can avoid frequent lookups and improve speed and reliability -->
  <xsl:variable name="host" select="encode-for-uri('https://oai.openedition.org')" />
  <xsl:variable name="set">journals:jtei</xsl:variable>
  <xsl:param name="metadata"
    select="doc('http://exist2.ulb.tu-darmstadt.de:8080/exist/restxq/xoai/listRecords?url=' || $host || '&amp;set=' || $set || '&amp;metadataPrefix=mets')"/>
  
  
  <xsl:import href="../variables.xsl" />
  
  <!-- remove PIs (esp. schema declarations) -->
  <xsl:template match="//processing-instruction()"/>

  <!-- ULB-Bf: author, editor contain standard name forms and no sub-elements; we assume that authors always are 
       persons - 2021-07-12 DK -->
  <xsl:template match="tei:teiHeader//tei:author">
    <author>
      <xsl:apply-templates select="tei:name" />
    </author>
  </xsl:template>
  <xsl:template match="tei:author/tei:name">
    <persName>
      <xsl:apply-templates select="tei:surname" />
      <xsl:apply-templates select="tei:forename" />
    </persName>
  </xsl:template>
  
  <xsl:template match="tei:surname">
    <xsl:apply-templates />
    <xsl:text>, </xsl:text>
  </xsl:template>
  <xsl:template match="tei:forename">
    <xsl:apply-templates />
  </xsl:template>
  
  <!-- ULB-Bf: editor (German: Herausgeber): respStmt/persName - 2021-07-12 DK -->
  <xsl:template match="tei:editor" xml:space="preserve">
              <persName
      ><xsl:apply-templates
    /></persName
  ></xsl:template>
  
  <!-- ULB-Bf: add a seriesStmt thath contains info about reprocessing and a publicationStmt; create sourceDesc from
       original title data - 2021-07-12 DK -->
  <xsl:template match="tei:fileDesc">
    <!-- get additional meta data from openeditions.org’s OAI-PMH using xOAI-client
       – https://oai.openedition.org/?verb=ListRecords&amp;set=journals:jtei&amp;metadataPrefix=mets -->
    <xsl:variable name="titles" as="xs:string+">
      <xsl:apply-templates select="tei:titleStmt/tei:title" mode="string"/>
    </xsl:variable>
    <xsl:variable name="title" select="normalize-space(string-join($titles, ': '))"/>
    <xsl:variable name="entry"
      select="$metadata//dcterms:title[normalize-space() eq $title]/parent::mets:xmlData"/>
    <xsl:variable name="issue" select="$entry/ancestor::mets:mets/mets:dmdSec[1]"/>
    
    <fileDesc xml:space="preserve">
      <xsl:apply-templates select="tei:titleStmt" />
      <xsl:sequence select="$ULBEditionStmt" />
      <publicationStmt>
        <xsl:sequence select="$ULBPublisher" />
        <!-- TODO idno einfügen -->
        <availability>
          <xsl:apply-templates select="//tei:publicationStmt/tei:availability/*" />
        </availability>
      </publicationStmt>
      <sourceDesc>
        <biblStruct>
          <analytic>
            <xsl:apply-templates select="//tei:titleStmt/tei:author" />
            <xsl:apply-templates select="//tei:titleStmt/tei:title" />
            <!-- get DOI via OAI-PMH from openeditions.org -->
            <idno type="DOI"
              ><xsl:value-of select="$entry/dcterms:identifier[starts-with(., 'urn:doi')]"
            /></idno>
            <ptr type="http://purl.org/vocab/frbr/core#transformationOf"
              target="{$entry/dcterms:hasFormat[@scheme eq 'TEI']}" />
          </analytic>
          <monogr>
            <!-- get the issue’s title from the openeditions data -->
            <title level="m"
              ><xsl:value-of select="$issue//dcterms:title[1]"
            /></title>
            <respStmt>
              <resp ref="http://id.loc.gov/vocabulary/relators/pbd"
              /><xsl:apply-templates select="//tei:seriesStmt/tei:editor[@role = ('chief', 'managing')]" />
            </respStmt>
            <xsl:if test="//tei:seriesStmt/tei:editor[not(@role = ('chief', 'managing'))]"
              ><respStmt>
              <resp ref="http://id.loc.gov/vocabulary/relators/edt"
              /><xsl:apply-templates select="//tei:seriesStmt/tei:editor[not(@role = ('chief', 'managing'))]" />
            </respStmt
            ></xsl:if>
            <xsl:apply-templates select="//tei:fileDesc/tei:publicationStmt" />
            <idno type="DOI"
              ><xsl:value-of select="$issue//dcterms:identifier[starts-with(., 'urn:doi')]"
            /></idno>
            <ptr target="{$issue//dcterms:identifier[starts-with(., 'http')]}" />
          </monogr>
          <series>
            <title level="j">Journal of the Text Encoding Initiative</title>
            <biblScope unit="issue"
              ><xsl:value-of select="//tei:seriesStmt/tei:biblScope/@n"
            /></biblScope>
            <idno type="eISSN"
              ><xsl:value-of select="$issue//dcterms:isPartOf[starts-with(., 'urn:eissn')]"
            /></idno>
          </series>
        </biblStruct>
      </sourceDesc>
    </fileDesc>
  </xsl:template>
  
  <!-- remove all attributs from TEI -->
  <xsl:template match="/tei:TEI">
    <TEI>
      <xsl:apply-templates/>
    </TEI>
  </xsl:template>
  
  <!-- titleStmt -->
  <xsl:template match="tei:titleStmt/tei:title">
    <title type="{@type}" level="a">
      <xsl:apply-templates/>
    </title>
  </xsl:template>
  <!-- evaluate title when searching for meta data, e.g. when there’s a quote in the title – 2021-08-26 DK;
       see also https://gitlab.ulb.tu-darmstadt.de/workflow-digitale-medien/konvertierungsskripte_wdm/-/issues/24 -->
  <xsl:template match="tei:title" mode="string" as="xs:string">
    <xsl:variable name="content">
      <xsl:apply-templates mode="#current" />
    </xsl:variable>
    <xsl:value-of select="string-join($content)" />
  </xsl:template>
  
  <xsl:template match="tei:q" mode="string">
    <xsl:text>“</xsl:text>
    <xsl:apply-templates />
    <xsl:text>”</xsl:text>
  </xsl:template>
  
  <!-- adapt publicationStmt as monogr/imprint - 2021-07-12 DK -->
  <xsl:template match="tei:publicationStmt" xml:space="preserve"
    ><imprint>
              <xsl:apply-templates select="tei:publisher" />
              <date when="{analyze-string(tei:date, '\d{4}')/*:match}"><xsl:value-of select="tei:date" /></date>
            </imprint
    ></xsl:template>
  
  <!-- seriesStmt: biblScope unused - 2021-08-26 DK -->
  <xsl:template match="tei:seriesStmt/tei:biblScope" />
  
  <!-- siehe basisformat_teiHeader_2020_11_17.xml Zeile 43 -->
  <!-- Remove role attributes from editor and author elements -->
  <xsl:template match="tei:editor/@role | tei:author/@role"/>
  
  <xsl:template match="tei:projectDesc">
    <!-- standard projectDesc anlegen. Vorlage entnommen aus basisformat_teiHeader_2020-11_17.xml -->
    <xsl:sequence select="$ULBprojectDesc"/>
    <xsl:sequence select="$ULB_schemaRef"/>
  </xsl:template>
  
  <xsl:template match="tei:textClass">
     <textClass>
      <classCode scheme="http://dewey.org">410</classCode>
      <xsl:apply-templates />
     </textClass> 
  </xsl:template>
  
  <xsl:template match="tei:revisionDesc" />
  
  <!-- fn:parse-xml prefixes all elements -->
  <xsl:template match="*">
    <xsl:element name="{local-name()}">
      <xsl:apply-templates select="@* | node()" />
    </xsl:element>
  </xsl:template>
  <xsl:template match="@* | text()">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>
</xsl:stylesheet>
